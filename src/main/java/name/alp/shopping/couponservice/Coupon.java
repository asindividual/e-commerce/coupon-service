package name.alp.shopping.couponservice;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.Objects;

@Document("coupon")
public class Coupon {
    @Id
    private String id;
    private String title;
    private Double discount;
    private Integer minimumAmountToUse;
    private DiscountType discountType;
    private Integer leftUsageCount;
    private boolean valid;
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private java.util.Date validUntil;

    public Coupon() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public Integer getLeftUsageCount() {
        return leftUsageCount;
    }

    public void setLeftUsageCount(Integer leftUsageCount) {
        this.leftUsageCount = leftUsageCount;
    }

    public Date getValidUntil() {
        return validUntil;
    }

    public void setValidUntil(Date validUntil) {
        this.validUntil = validUntil;
    }

    public Double getDiscount() {
        return discount;
    }

    public void setDiscount(Double discount) {
        this.discount = discount;
    }

    public DiscountType getDiscountType() {
        return discountType;
    }

    public void setDiscountType(DiscountType discountType) {
        this.discountType = discountType;
    }

    public Integer getMinimumAmountToUse() {
        return minimumAmountToUse;
    }

    public void setMinimumAmountToUse(Integer minimumAmountToUse) {
        this.minimumAmountToUse = minimumAmountToUse;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Coupon coupon = (Coupon) o;
        return id.equals(coupon.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
