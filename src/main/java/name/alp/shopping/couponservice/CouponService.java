package name.alp.shopping.couponservice;

import org.springframework.stereotype.Service;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class CouponService {

    private final CouponRepository couponRepository;

    public CouponService(CouponRepository couponRepository) {
        this.couponRepository = couponRepository;
    }

    public Mono<Coupon> save(Coupon coupon) {
        return couponRepository.save(coupon);
    }

    public Mono<Coupon> findById(String id) {
        return couponRepository.findById(id);
    }

    public Boolean validate(String id, Date validityDate) {

        Mono<Coupon> coupon= couponRepository.validate(id,validityDate);
        AtomicReference<Coupon> atomicCoupon = new AtomicReference<>();
        Disposable subscribe = coupon.subscribe(couponDBO -> {
            atomicCoupon.set(couponDBO);
        });

        while (!subscribe.isDisposed()){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        return (atomicCoupon.get()!=null);
    }

    public Mono<Coupon> useCoupon(String id, Date validityDate) {

        Mono<Coupon> coupon= couponRepository.validate(id,validityDate);
        AtomicReference<Coupon> atomicCoupon = new AtomicReference<>();
        Disposable subscribe = coupon.subscribe(couponDBO -> {
            atomicCoupon.set(couponDBO);
        });
        while (!subscribe.isDisposed()){
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        Coupon usedCoupon = atomicCoupon.get();
        usedCoupon.setLeftUsageCount(usedCoupon.getLeftUsageCount()-1);
        return save(usedCoupon);
    }

}
