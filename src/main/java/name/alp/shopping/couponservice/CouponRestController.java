package name.alp.shopping.couponservice;

import io.swagger.v3.oas.annotations.Operation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Date;

@RestController
public class CouponRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CouponRestController.class);
    private final CouponService couponService;

    public CouponRestController(CouponService couponService) {
        this.couponService = couponService;
    }

    @Operation(summary = "Provides save function for the argument",
            description = "This endpoint creates a new record if the argument doesnt have an id, if the argument has a proper id than it updates" )
    @PostMapping
    public Mono<Coupon> save(@RequestBody Coupon coupon) {
        LOGGER.info("create: {}", coupon);
        return couponService.save(coupon);
    }

    @Operation(summary = "Provides an entity for the specified id",
            description = "")
    @GetMapping("/{id}")
    public Mono<Coupon> findById(@PathVariable("id") String id) {
        LOGGER.info("findById: id={}", id);
        return couponService.findById(id);
    }

    @Operation(summary = "Validate Coupon for the specified id",
            description = "")
    @GetMapping("/validate/{id}")
    public Boolean validate(@PathVariable("id") String id) {
        LOGGER.info("findById: id={}", id);
        return couponService.validate(id,new Date());
    }

    @Operation(summary = "Use Coupon for the specified id",
            description = "")
    @GetMapping("/useCoupon/{id}")
    public Mono<Coupon> useCoupon(@PathVariable("id") String id) {
        LOGGER.info("findById: id={}", id);
        return couponService.useCoupon(id , new Date());
    }

}
