package name.alp.shopping.couponservice;

import org.springframework.data.mongodb.repository.Query;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CouponRepository extends ReactiveMongoRepository<Coupon ,String> {
    @Query("{ " +
                " 'id': ?#{[0]}  , " +
                " 'valid' : true ," +
                " 'validUntil' : { '$gte' : ?#{[1]}  } ," +
                " 'leftUsageCount' : { '$gt' : 0 }" +
            " }")
    Mono<Coupon> validate(String id,   java.util.Date validUntil);
}
