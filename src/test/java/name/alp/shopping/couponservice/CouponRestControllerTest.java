package name.alp.shopping.couponservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.web.reactive.function.BodyInserters;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


import static org.mockito.Mockito.times;


@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CouponRestController.class)
@Import(CouponService.class)
public class CouponRestControllerTest {

    @MockBean
    CouponRepository repository;

    @Autowired
    private WebTestClient webClient;

    @Test
    void testSave() {
        Coupon coupon = new Coupon();
        coupon.setId("test_id1");
        coupon.setTitle("Test title");


        Mockito.when(repository.save(coupon)).thenReturn(Mono.just(coupon));

        webClient.post()
                .uri("/")
                .contentType(MediaType.APPLICATION_JSON)
                .body(BodyInserters.fromObject(coupon))
                .exchange()
                .expectStatus().isOk();

    }

    @Test
    void testFindById()
    {
        Coupon coupon = new Coupon();
        coupon.setId("test_id1");
        coupon.setTitle("Test title");

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(coupon));

        webClient.get().uri("/{id}", "test_id1")
                .exchange()
                .expectStatus().isOk()
                .expectBody()
                .jsonPath("$.title").isNotEmpty()
                .jsonPath("$.id").isEqualTo("test_id1");

        Mockito.verify(repository, times(1)).findById("test_id1");
    }



}
