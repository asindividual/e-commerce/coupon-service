package name.alp.shopping.couponservice;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Calendar;
import java.util.Date;

import static org.assertj.core.api.Assertions.*;

@ExtendWith(SpringExtension.class)
@WebFluxTest(controllers = CouponRestController.class)
@Import(CouponService.class)
public class CouponServiceTest {

    @Autowired
    private  CouponService couponService;


    @MockBean
    CouponRepository repository;

    @Test
    public void saveTest(){
        Coupon coupon = new Coupon();
        coupon.setId("test_id1");
        coupon.setTitle("Test title");
        Mockito.when(repository.save(coupon)).thenReturn(Mono.just(coupon));

        Mono<Coupon> result =  couponService.save(coupon);

        assertThat(result).isNotNull();
    }

    @Test
    public void findByIdTest(){
        Coupon coupon = new Coupon();
        coupon.setId("test_id1");
        coupon.setTitle("Test title");

        Mockito
                .when(repository.findById("test_id1"))
                .thenReturn(Mono.just(coupon));

        Mono<Coupon> result =  couponService.findById("test_id1");
        assertThat(result).isNotNull();
    }

    @Test
    public void validateTest(){
        Coupon coupon = new Coupon();
        coupon.setId("test_id1");
        coupon.setTitle("Test title");
        coupon.setLeftUsageCount(1);

        Date date=new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        coupon.setValidUntil(c.getTime());

        Mockito
                .when(repository.validate("test_id1",date))
                .thenReturn(Mono.just(coupon));

        boolean result =  couponService.validate("test_id1",date);
        assertThat(result).isEqualTo(true);
    }

    @Test
    public void useCouponTest(){
        Coupon coupon = new Coupon();
        coupon.setId("test_id1");
        coupon.setTitle("Test title");
        coupon.setLeftUsageCount(1);

        Date date=new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DATE, 1);
        coupon.setValidUntil(c.getTime());

        Mockito
                .when(repository.validate("test_id1",date))
                .thenReturn(Mono.just(coupon));
        Coupon expected=new Coupon();
        expected.setId("test_id1");
        expected.setTitle("Test title");
        expected.setLeftUsageCount(0);

        Mockito
                .when(repository.save(coupon))
                .thenReturn(Mono.just(expected));

        Mono<Coupon> result =  couponService.useCoupon("test_id1",date);
        assertThat(result.block().getLeftUsageCount()).isEqualTo(0);
    }



}
